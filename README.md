cordova-imagePicker
===================

Плагин для Ionic Framework позволяет выбрать несколько фотографий из галлереи. 
Форк плагина https://github.com/Telerik-Verified-Plugins/ImagePicker 
добавлен интерфейс на русском

## Установка плагина

```console
npm install @ionic-native/image-picker
ionic cordova plugin add https://xsfera@bitbucket.org/xsfera/imagepicker.git

со своим текстом запроса доступа к галереи
ionic cordova plugin add https://xsfera@bitbucket.org/xsfera/imagepicker.git --variable PHOTO_LIBRARY_USAGE_DESCRIPTION="Нужен доступ к галлереи для загрузки фото."
```

## Пример использования

```javascript
import { ImagePicker, ImagePickerOptions } from '@ionic-native/image-picker/ngx';

    const options: ImagePickerOptions = {
      maximumImagesCount: this.count - this.photoFiles.length,
      height: 600,
      width: 600,
      quality: 85,
      outputType: 1,
      title: 'Выбор фото'
    };

    this.imagePicker.getPictures(options).then((imageData) => {
      // в imageData Base64 картинки, можно сменить параметром outputType на url файла
    });
```

### Options

    options = {
        // Максимальное количество разрешенных на выбор фото
        maximumImagesCount: int,
        
        // Максимальная высота/ширина фото,
        // загружаемая фотография будет уменьшена на устройстве.
        width: int,
        height: int,
        
        // Качество картинки и ее сжатие
        quality: int (0-100),

        // тип передеваемого результата, по умолчанию 0
        // 0 - FILE_URI или
        // 1 - BASE64
        outputType: int
    };
    
## Использованные бибилиотеки

#### ELCImagePicker

For iOS this plugin uses the ELCImagePickerController, with slight modifications for the iOS image picker.  ELCImagePicker uses the MIT License which can be found in the file LICENSE.

https://github.com/B-Sides/ELCImagePickerController

#### MultiImageChooser

For Android this plugin uses MultiImageChooser, with modifications.  MultiImageChooser uses the BSD 2-Clause License which can be found in the file BSD_LICENSE.  Some code inside MultImageChooser is licensed under the Apache license which can be found in the file APACHE_LICENSE.

https://github.com/derosa/MultiImageChooser

#### FakeR

Code(FakeR) was also taken from the phonegap BarCodeScanner plugin.  This code uses the MIT license.

https://github.com/wildabeast/BarcodeScanner

## License

The MIT License

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
